// Append buttons to add every product
var fetchProductsButton = document.getElementById('fetchProduct');
var autoFetchProductsButton = document.getElementById('autoFetchProduct');
var addCurrentProductButton = document.getElementById('addCurrentProduct');
var batchAddProductButton = document.getElementById('batchAddProduct');

fetchProductsButton.addEventListener('click', (event) => {
    event.preventDefault();

    console.log('Generating add buttons...');

    chrome.tabs.getSelected(null, (tab) => {
        chrome.tabs.executeScript(
            tab.id,
            {
                code: '(' + appendAddButton + ')();'
            }
        )
    });
}, false);

autoFetchProductsButton.addEventListener('click', (event) => {
    event.preventDefault();

    console.log('Auto fetching products...');

    autoFetchProducts();
})

addCurrentProductButton.addEventListener('click', (event) => {
    event.preventDefault();
    getCurrentProduct();
}, false);

batchAddProductButton.addEventListener('click', (event) => {
    event.preventDefault();
    batchAddProduct();
}, false);

// Append add button to each product
function appendAddButton() {
    let products = document.querySelectorAll('#s-results-list-atf > .s-result-item');
    if (products.length < 1) {
        products = document.querySelectorAll('.s-result-list > .s-result-item');
    }
    
    console.log(1);

    products.forEach((product, index) => {
        let _link, _asin, _originalAsin, _kindlePrice;
        try {
            _link = product.querySelector('a[title*="Kindle"]').getAttribute('href');
            if (_link.search('https://www.amazon.com') == -1) {
                _link = 'https://www.amazon.com' + _link;
            }
        } catch {
            try {
                _link = product.querySelector('a[href*="ebook"]').href;
                if (_link.search('https://www.amazon.com') == -1) {
                    _link = 'https://www.amazon.com' + _link;
                }
            } catch {
                _link = 'not-available';
            }
        }

        try {
            _asin = _link.match(new RegExp('dp/' + '(.*)' + '/ref'))[1];
        } catch {
            _asin = 'Error';
        }

        try {
            _originalAsin = product.getAttribute('data-asin');
        } catch {
            _originalAsin = 'Error';
        }

        try {
            _kindlePrice = document.evaluate('//a[contains(@href, "' + _asin + '")]//span[contains(@class, "a-offscreen")]' , document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.innerText;
            _kindlePrice = parseFloat(_kindlePrice.replace('$', ''));
        } catch {
            _price = 0;
        }
        
        if (_link == '' || _asin =='') {
            return;
        } else {
            window.postMessage({
                type: "check-product",
                link: _link,
                asin: _asin,
                originalAsin: _originalAsin,
                kindlePrice: _kindlePrice,
                id: index
            }, "*");
        }
    });
};

function autoFetchProducts() {
    chrome.runtime.sendMessage({
        data: {
            type: 'auto-add-product',
        }
    });
}

function getCurrentProduct() {
    let _customPrice = document.getElementById('custom-price-input').value;

    if (_customPrice.length < 1) {
        alert('Invalid price!');
    } else {
        chrome.runtime.sendMessage({
            data: {
                type: 'add-current-product-contentjs'
            }
        });
    }
};

function batchAddProduct() {
    let _isbnList = document.getElementById('isbn-list').value;
    let _batchCustomPrice = document.getElementById('custom-batch-price-input').value;
    
    if (_isbnList.length < 14) {
        alert('Please enter ISBN-13 list with the format: [ISBN 1],[ISBN 2],...');
    } else {
        try {
            _isbnList = _isbnList.split('\n');
            chrome.runtime.sendMessage({
                data: {
                    type: 'batch-add-product',
                    isbnList: _isbnList,
                    batchCustomPrice: _batchCustomPrice
                }
            });
        } catch {
            alert('Invalid list!');
            console.log("Invalid list!");
        }
    }
}