// API
productAPI = "http://157.230.32.49/api/product";

addFlag = false;
addTimeout = 3000;

autoAddFlag = false;

chrome.runtime.onMessage.addListener(function action(request, sender, sendResponse) {
    if (request.data.type == 'check-product') {
        request.data.type = 'check-response';
        
        if (request.data.link == 'not-available') {
            request.data.flag = 'invalid';

            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                chrome.tabs.sendMessage(
                    tabs[0].id,
                    {
                        data: request.data
                    }
                );
            });
        } else {
            let xhrGet = new XMLHttpRequest();
            xhrGet.open('GET', productAPI + '?asin=' + request.data.asin);
            xhrGet.send();

            xhrGet.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    let data = JSON.parse(this.responseText);

                    if (data.length > 0) {
                        request.data.flag = 'invalid';
                    } else {
                        request.data.flag = 'valid';
                    }

                    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                        chrome.tabs.sendMessage(
                            tabs[0].id,
                            {
                                data: request.data
                            }
                        );
                    });
                }
            };
        }
    }
    
    if (request.data.type == 'add-product') {
        console.log("Adding product to database...")

        if (addFlag) {
            console.log("Too fast.");
        } else {
            addFlag = true;
            setTimeout(() => {
                addFlag = false;
            }, addTimeout);

            chrome.tabs.create(
                { 
                    url: request.data.link,
                    active: false
                },
                (tab) => {
                    setTimeout(() => {
                        console.log("Start fetching data...");

                        chrome.tabs.executeScript(
                            tab.id,
                            {
                                code: '(' + fetchData + ')();'
                            },
                            (result) => {
                                var product = result[0];

                                // ---
                                // product = reformatResult(product);
                                // product.price = parseFloat(request.data.customPrice);
                                // console.log(product);

                                // chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                                //     chrome.tabs.sendMessage(
                                //         tabs[0].id,
                                //         {
                                //             data: {
                                //                 type: 'auto-add-product-continue-contentjs'
                                //             }
                                //         }
                                //     );
                                // });

                                // Close tab
                                chrome.tabs.executeScript(
                                    tab.id,
                                    {
                                        code: 'window.close();'
                                    },
                                    (result) => {
                                        console.log('Finished fetching data.');
                                    }
                                );

                                // GET to check if product is already added
                                let xhrGet = new XMLHttpRequest();
                                xhrGet.open('GET', productAPI + '?isbn=' + product.isbn);
                                xhrGet.send();

                                xhrGet.onreadystatechange = function() {
                                    if (this.readyState == 4 && this.status == 200) {
                                        let data = JSON.parse(this.responseText);

                                        if (data.length > 0) {
                                            if (autoAddFlag) {
                                                console.log('Product already added!');
                                                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                                                    chrome.tabs.sendMessage(
                                                        tabs[0].id,
                                                        {
                                                            data: {
                                                                type: 'auto-add-product-continue-contentjs'
                                                            }
                                                        }
                                                    );
                                                });
                                            } else {
                                                chrome.tabs.getSelected(null, (currentTab) => {
                                                    chrome.tabs.executeScript(
                                                        currentTab.id,
                                                        {
                                                            code: 'tempProduct = document.querySelector("button.af-add-product[data-asin=\'' + request.data.asin + '\']"); tempProduct.innerText = "Product Added"; tempProduct.style.pointerEvents = "none"; try { document.querySelector("#s-results-list-atf > *[data-asin=\'' + request.data.originalAsin + '\']").style.backgroundColor = "rgba(255, 42, 42, 0.5)"; } catch { try { document.querySelector(".s-result-list > *[data-asin=\'' + request.data.originalAsin + '\']").style.backgroundColor = "rgba(255, 42, 42, 0.5)"; } catch { console.log("Unresolved condition"); } }; alert("Product already added!");'
                                                        },
                                                        (result) => {
                                                            console.log('Product already added!');
                                                        }
                                                    );
                                                });
                                            }
                                        } else {
                                            product = reformatResult(product);
                                            product.price = parseFloat(request.data.customPrice);
                                            console.log(product);

                                            // POST data to DB through API
                                            let xhrPost = new XMLHttpRequest();
                                            xhrPost.open('POST', productAPI);
                                            xhrPost.setRequestHeader('Content-Type', 'application/json');
                                            xhrPost.send(JSON.stringify({                            
                                                'title': product.title,
                                                'author': product.authors,
                                                'image_url': product.imgLink,
                                                'isbn': product.isbn,
                                                'asin': request.data.asin,
                                                'catalog': product.categories,
                                                'price': product.price,
                                                'description': product.desc,
                                                'product_reviews': product.reviews,
                                                'product_type': 1
                                            }));

                                            xhrPost.onreadystatechange = function() {
                                                console.log(this);
                                                if (this.readyState == 4 && this.status == 201) { 
                                                    if (autoAddFlag) {
                                                        console.log('Product added!');
                                                        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                                                            chrome.tabs.sendMessage(
                                                                tabs[0].id,
                                                                {
                                                                    data: {
                                                                        type: 'auto-add-product-continue-contentjs'
                                                                    }
                                                                }
                                                            );
                                                        });
                                                    } else {
                                                        chrome.tabs.getSelected(null, (currentTab) => {
                                                            chrome.tabs.executeScript(
                                                                currentTab.id,
                                                                {
                                                                    code: 'tempProduct = document.querySelector("button.af-add-product[data-asin=\'' + request.data.asin + '\']"); tempProduct.innerText = "Product Added"; tempProduct.style.pointerEvents = "none"; try { document.querySelector("#s-results-list-atf > *[data-asin=\'' + request.data.originalAsin + '\']").style.backgroundColor = "rgba(255, 42, 42, 0.5)"; } catch { try { document.querySelector(".s-result-list > *[data-asin=\'' + request.data.originalAsin + '\']").style.backgroundColor = "rgba(255, 42, 42, 0.5)"; } catch { console.log("Unresolved condition"); } }; alert("Product added!"); console.log("Product added.");'
                                                                },
                                                                (result) => {
                                                                    console.log('Product added!')
                                                                }
                                                            );
                                                        });
                                                    }
                                                }
                                            };
                                        }
                                    }
                                };
                            }

                        );
                    }, 3000);         
                }
            );
        }
    }

    if (request.data.type == 'auto-add-product') {
        autoAddFlag = true;

        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(
                tabs[0].id,
                {
                    data: {
                        type: 'auto-add-product-contentjs'
                    }
                }
            );
        });
    }

    if (request.data.type == 'auto-add-product-next-page') {
        setTimeout(() => {
            console.log('---\n\nNext page!\n\n---');
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                chrome.tabs.sendMessage(
                    tabs[0].id,
                    {
                        data: {
                            type: 'auto-add-product-contentjs'
                        }
                    }
                );
            });
        }, 10000);
    }

    if (request.data.type == 'auto-add-product-stop') {
        autoAddFlag = false;
    }

    if (request.data.type == 'add-current-product') {
        chrome.tabs.getSelected(null, (currentTab) => {
            chrome.tabs.executeScript(
                currentTab.id,
                {
                    code: '(' + fetchData + ')();'
                },
                (result) => {
                    var product = result[0];
    
                    // product = reformatResult(product);
                    // product.price = parseFloat(request.data.customPrice);
                    // console.log(product);
    
                    // GET to check if product is already added
                    let xhrGet = new XMLHttpRequest();
                    xhrGet.open('GET', productAPI + '?isbn=' + product.isbn);
                    xhrGet.send();
    
                    xhrGet.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            let data = JSON.parse(this.responseText);
    
                            if (data.length > 0) {
                                chrome.tabs.getSelected(null, (currentTab) => {
                                    chrome.tabs.executeScript(
                                        currentTab.id,
                                        {
                                            code: 'alert("Product already added!");'
                                        },
                                        (result) => {
                                            console.log('Product already added!');
                                        }
                                    );
                                });
                            } else {
                                product = reformatResult(product);
                                product.price = parseFloat(request.data.customPrice);
                                console.log(product);
    
                                // POST data to DB through API
                                let xhrPost = new XMLHttpRequest();
                                xhrPost.open('POST', productAPI);
                                xhrPost.setRequestHeader('Content-Type', 'application/json');
                                xhrPost.send(JSON.stringify({                            
                                    'title': product.title,
                                    'author': product.authors,
                                    'image_url': product.imgLink,
                                    'isbn': product.isbn,
                                    'asin': request.data.asin,
                                    'catalog': product.categories,
                                    'price': product.price,
                                    'description': product.desc,
                                    'product_reviews': product.reviews,
                                    'product_type': 1
                                }));
    
                                xhrPost.onreadystatechange = function() {
                                    console.log(this);
                                    if (this.readyState == 4 && this.status == 201) { 
                                        chrome.tabs.getSelected(null, (currentTab) => {
                                            chrome.tabs.executeScript(
                                                currentTab.id,
                                                {
                                                    code: 'alert("Product added!"); console.log("Product added.");'
                                                },
                                                (result) => {
                                                    console.log('Product added!')
                                                }
                                            );
                                        });
                                    }
                                };
                            }
                        }
                    };
                }
            );
        });
    }

    if (request.data.type == 'batch-add-product') {
        var isbnList = request.data.isbnList;
        var batchCustomPrice = parseFloat(request.data.batchCustomPrice);
        var currentIndex = 0;
        console.log(isbnList);
        console.log('Custom price', batchCustomPrice);

        function recursiveFetchFromSearch(isbnList, currentIndex) {
            if (currentIndex >= isbnList.length) {
                return;
            }

            var currentISBN = isbnList[currentIndex];
            console.log(currentISBN);
            
            chrome.tabs.getSelected(null, (currentTab) => {
                chrome.tabs.executeScript(
                    currentTab.id,
                    {
                        code: 'window.location="https://www.amazon.com/s?k=' + currentISBN + '&ref=nb_sb_noss"'
                    },
                    (result) => {
                        setTimeout(() => {
                            chrome.tabs.executeScript(
                                currentTab.id,
                                {
                                    code: '(' + fetchFromSearchResult + ')();'
                                },
                                (result) => {
                                    console.log('Search result', result);

                                    if (result[0] == 'none' || result[0] == 'error') {
                                        console.log('Error');
                                        setTimeout(() => {
                                            recursiveFetchFromSearch(isbnList, currentIndex + 1);
                                        }, 10000)   
                                    } else {
                                        var price = result[0].price;

                                        chrome.tabs.create(
                                            { 
                                                url: result[0].link,
                                                active: false
                                            },
                                            (tab) => {
                                                setTimeout(() => {
                                                    console.log("Start fetching data...");
                            
                                                    chrome.tabs.executeScript(
                                                        tab.id,
                                                        {
                                                            code: '(' + fetchData + ')();'
                                                        },
                                                        (result) => {
                                                            var product = result[0];
        
                                                            // product = reformatResult(product);
                                                            // product.price = parseFloat(request.data.customPrice);
                                                            // console.log(product);

                                                            // Close tab
                                                            chrome.tabs.executeScript(
                                                                tab.id,
                                                                {
                                                                    code: 'window.close();'
                                                                },
                                                                (result) => {
                                                                    console.log('Finished fetching data.');
                                                                }
                                                            );
                                            
                                                            // GET to check if product is already added
                                                            let xhrGet = new XMLHttpRequest();
                                                            xhrGet.open('GET', productAPI + '?isbn=' + product.isbn + '&type=2');
                                                            xhrGet.send();
                                            
                                                            xhrGet.onreadystatechange = function() {
                                                                if (this.readyState == 4 && this.status == 200) {
                                                                    let data = JSON.parse(this.responseText);
                                            
                                                                    if (data.length > 0) {
                                                                        chrome.tabs.getSelected(null, (currentTab) => {
                                                                            chrome.tabs.executeScript(
                                                                                currentTab.id,
                                                                                {
                                                                                    code: 'console.log("Product already added!");'
                                                                                },
                                                                                (result) => {
                                                                                    console.log('Product already added!');
                                                                                }
                                                                            );
                                                                        });
                                                                    } else {
                                                                        product = reformatResult(product);
                                                                        if (batchCustomPrice > 0) {
                                                                            product.price = batchCustomPrice;
                                                                        } else {
                                                                            product.price = parseFloat(price);
                                                                        }
                                                                        console.log(product);
                                            
                                                                        // POST data to DB through API
                                                                        let xhrPost = new XMLHttpRequest();
                                                                        xhrPost.open('POST', productAPI);
                                                                        xhrPost.setRequestHeader('Content-Type', 'application/json');
                                                                        xhrPost.send(JSON.stringify({                            
                                                                            'title': product.title,
                                                                            'author': product.authors,
                                                                            'image_url': product.imgLink,
                                                                            'isbn': product.isbn,
                                                                            'asin': 'not-set',
                                                                            'catalog': product.categories,
                                                                            'price': product.price,
                                                                            'description': product.desc,
                                                                            'product_reviews': product.reviews,
                                                                            'product_type': 2
                                                                        }));
                                            
                                                                        xhrPost.onreadystatechange = function() {
                                                                            console.log(this);
                                                                            if (this.readyState == 4 && this.status == 201) { 
                                                                                chrome.tabs.getSelected(null, (currentTab) => {
                                                                                    chrome.tabs.executeScript(
                                                                                        currentTab.id,
                                                                                        {
                                                                                            code: 'console.log("Product added.");'
                                                                                        },
                                                                                        (result) => {
                                                                                            console.log('Product added!')
                                                                                        }
                                                                                    );
                                                                                });
                                                                            }
                                                                        };
                                                                    }
                                                                }
                                                            };

                                                            recursiveFetchFromSearch(isbnList, currentIndex + 1);
                                                        }
                                                    );
                                                }, 3000);
                                            }
                                        );
                                    }
                                }
                            );
                        }, 3000);
                    }
                );
            });
        };

        recursiveFetchFromSearch(isbnList, currentIndex);
    }

    sendResponse();
});

 // Get single product
function fetchData() {
    let _title;
    try {
        _title = document.querySelector('#title').innerText;
        _title = _title.replace(',Kindle Edition', '');
        _title = _title.replace('Kindle Edition', '');
    } catch {
        _title = '';
    }

    let _desc = '';

    let _isbn;
    try {
        _isbn = document.evaluate('//table[@id="productDetailsTable"]//li//b[contains(text(), "ISBN-13")]/parent::*' , document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.innerText;
        _desc += _isbn;
        _isbn = _isbn.replace('ISBN-13: ', '');
    } catch {
        try {
            _isbn = document.evaluate('//div[@id="printEditionIsbn_feature_div"]//span[contains(text(), "ISBN-13")]/parent::*' , document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.innerText;
            _desc += _isbn;
            _isbn = _isbn.replace('ISBN-13: ', '');
        } catch {
            try {
                _isbn = document.evaluate('//table[@id="productDetailsTable"]//li//b[contains(text(), "ASIN")]/parent::*' , document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.innerText;
                _isbn = _isbn.replace('ASIN: ', '');
            } catch {
                _isbn = '';
            }
        }
    }
    _desc += '\n';

    let _authors = [];
    try {
        _desc += 'Author: ';
        document.querySelectorAll('#bylineInfo span.author').forEach((object, index) => {
            let contribution = object.querySelector('span.contribution').innerText;
            _authors[index] = object.innerText.replace(contribution, '');
            _authors[index] = _authors[index].trim();

            _desc += _authors[index] + ', ';
        });
    } catch {

    }
    _desc = _desc.slice(0, _desc.length - 2);

    let _imgLink = 0;
    try {
        let imgObjString = document.querySelector('#ebooksImgBlkFront').getAttribute('data-a-dynamic-image');
        let imgObj = JSON.parse(imgObjString);
        let keyNames = Object.keys(imgObj)
        _imgLink = keyNames[0];
    } catch {
        try {
            let imgObjString = document.querySelector('#imgBlkFront').getAttribute('data-a-dynamic-image');
            let imgObj = JSON.parse(imgObjString);
            let keyNames = Object.keys(imgObj)
            _imgLink = keyNames[0];
        } catch {
            _imgLink = 'Error';
        }
    }

    // let _kindlePrice;
    // try {
    //     _kindlePrice = document.evaluate('//li[contains(@class, "swatchElement")]//span[contains(text(), "Kindle")]/parent::a//span[contains(@class, "a-color-price")]' , document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.innerText;
    //     _kindlePrice = parseFloat(_kindlePrice.replace('$', ''));
    // } catch {
    //     _kindlePrice = 0;
    // }

    let _categories = [];
    try {
        document.querySelectorAll("#productDetailsTable #SalesRank > ul > li > span:last-child").forEach((object, index) => {
            _categories[index] = object.innerText.slice(3);
            _categories[index] = _categories[index].replace(/ > /g, ',');
        });

        if (_categories.length < 1) {
            _categories[0] = 'Uncategorized';
        }
    } catch {
        _categories[0] = 'Uncategorized';
    }

    let _reviews = [];
    try {
        document.querySelectorAll('#cm-cr-dp-review-list > .review').forEach((review, index) => {
            if (review.querySelector('i.a-star-5')) {
                let _reviewer = review.querySelector('.a-profile .a-profile-name').innerText;
            
                let _title = review.querySelector('.review-title').innerText;
                
                let _content = review.querySelector('.review-text').innerText;
                _content = _content.replace('\nRead more\n', '');

                _reviews.push({
                    reviewer: _reviewer,
                    title: _title,
                    content: _content
                });
            }
        });
    } catch {

    }

    let productData = {
        title: _title,
        authors: _authors,
        imgLink: _imgLink,
        // kindlePrice: _kindlePrice,
        desc: _desc,
        isbn: _isbn,
        categories: _categories,
        reviews: _reviews
    };

    return productData;
}

function fetchFromSearchResult() {
    try {
        results = document.querySelectorAll('#s-results-list-atf > .s-result-item');
        if (results.length < 1) {
            results = document.querySelectorAll('.s-result-list > .s-result-item');
        }
    } catch {
        return 'none';
    }

    try {
        let result = results[0];
        
        let _link = result.querySelector('a.a-link-normal').href;
        if (_link.search('https://www.amazon.com') == -1) {
            _link = 'https://www.amazon.com' + _link;
        }

        let _price = result.querySelector('span.a-offscreen').innerText;
        _price = _price.replace('$', '');

        return {
            link: _link,
            price: _price
        }
    } catch {
        return 'error';
    }
}

function reformatResult(product) {
    let authors = '';
    for (i = 0; i < product.authors.length; i++) {
        authors += product.authors[i];
        if (i != product.authors.length - 1) {
            authors += ',';
        }
    }

    let categoriesString = '';
    for (i = 0; i < product.categories.length; i++) {
        categoriesString += product.categories[i];
        if (i != product.categories.length - 1) {
            categoriesString += ',';
        }
    }
    let catalog = {
        'name': categoriesString
    }

    product.authors = authors;
    product.categories = catalog;

    return product;
}