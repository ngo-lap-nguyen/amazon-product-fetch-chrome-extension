window.onload = () => {
    // Sending check for kindle product to popup
    let _kindlePrice = 0;
        
    try {
        _kindlePrice = document.evaluate('//tr[contains(@class, "kindle-price")]//td[contains(text(), "$")]' , document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.innerText.replace('$', '');
        _kindlePrice = parseFloat(_kindlePrice);
    } catch {
        try {
            _kindlePrice = document.evaluate('//div[contains(@id, "mediaNoAccordion")]//span[contains(@class, "a-color-price")]' , document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.innerText.replace('$', '');
            _kindlePrice = parseFloat(_kindlePrice);
        } catch {
            _kindlePrice = -1
        }
    }

    // ---
    var products;
    var autoAddIndex = 0;

    setTimeout(() => {
        window.addEventListener('message', (event) => {
            event.preventDefault();
            event.stopPropagation();
        
            if (event.data.type == 'add-product') {
                chrome.runtime.sendMessage({
                    data: event.data
                });
            }
    
            if (event.data.type == 'check-product') {
                console.log(2);
                products = document.querySelectorAll('#s-results-list-atf > .s-result-item');
                if (products.length < 1) {
                    products = document.querySelectorAll('.s-result-list > .s-result-item');
                }
                chrome.runtime.sendMessage({
                    data: event.data
                });
            }
        });
    
        // ---    
        chrome.runtime.onMessage.addListener(function action(request, sender, sendResponse) {
            console.log(3, request);

            if (request.data.type == 'check-response') {
                function htmlToElement(html) {
                    var template = document.createElement('template');
                    html = html.trim(); // Never return a text node of whitespace as the result
                    template.innerHTML = html;
                    return template.content.firstChild;
                };
    
                // ---  
                let bCheck = document.querySelector('button.af-add-product[data-asin="' + request.data.asin + '"]');
                if (bCheck) {
                    return;
                }

                console.log(request.data.flag, request.data.id);
                if (request.data.flag == 'invalid') {
                    products[request.data.id].style.backgroundColor = 'rgba(255, 42, 42, 0.5)';
                } else {
                    let innerContainer = products[request.data.id].querySelector('.sg-col-inner');
                    if (!innerContainer) {
                        innerContainer = products[request.data.id];
                    }

                    let priceInput = htmlToElement('<input type="text" name="af-price-input" class="af-price-input" value="' + request.data.kindlePrice + '" style="margin-top: -90px; z-index: 999999; height: 41px; float: right;" data-asin="' + request.data.asin + '">');
                    innerContainer.appendChild(priceInput);

                    let newButton = htmlToElement('<button class="af-add-product" style="margin-top: -90px; z-index: 999999; float: right; padding: 10px;" data-href="' + request.data.link + '" data-asin="' + request.data.asin + '" data-original-asin="' + request.data.originalAsin + '">Add Product</button>');
                    innerContainer.appendChild(newButton);
                    
                    newButton.addEventListener('click', (event) => {
                        event.preventDefault();
                        event.stopPropagation();
            
                        console.log("Adding product...");
            
                        let _link = event.target.getAttribute('data-href');
                        if (_link.search('https://www.amazon.com') == -1) {
                            _link = 'https://www.amazon.com' + _link;
                        }

                        let _asin = event.target.getAttribute('data-asin');
                        let _originalAsin = event.target.getAttribute('data-original-asin');
                        let _customPrice = document.querySelector('input.af-price-input[data-asin="' + _asin + '"]').value;

                        window.postMessage({
                            type: "add-product",
                            link: _link,
                            asin: _asin,
                            originalAsin: _originalAsin,
                            customPrice: _customPrice
                        }, "*");
                    }, false);
                }
            }
            
            if (request.data.type == 'add-current-product-contentjs') {
                let _asin = ''
                
                try {
                    _asin = document.evaluate('//table[@id="productDetailsTable"]//li//b[contains(text(), "ASIN")]/parent::*' , document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue.innerText;
                    _asin = _asin.replace('ASIN: ', '');
                } catch {
                    try {
                        _asin = window.location.href.match(new RegExp('dp/' + '(.*)' + '/ref'))[1];
                    } catch {
                        _asin = ''
                    }
                }

                chrome.runtime.sendMessage({
                    data: {
                        type: 'add-current-product',
                        asin: _asin,
                        customPrice: request.data.customPrice
                    }
                })
            }

            // If the request is to auto add products
            // Initialize the products array and the auto add process
            if (request.data.type == 'auto-add-product-contentjs') {
                products = document.querySelectorAll('#s-results-list-atf > .s-result-item');
                if (products.length < 1) {
                    products = document.querySelectorAll('.s-result-list > .s-result-item');
                }

                console.log(products);

                autoAddIndex = -1;
                autoAddProduct();
            }

            if (request.data.type == 'auto-add-product-continue-contentjs') {
                console.log('Next');
                autoAddProduct();
            }
        });
    }, 1500);

    function autoAddProduct() {
        autoAddIndex += 1;

        if (autoAddIndex >= products.length) {
            // Send signal that we are moving to the next page
            chrome.runtime.sendMessage({
                data: {
                    type: 'auto-add-product-next-page'
                }
            });
            
            // Move to the next page
            let nextPageButton = document.querySelector('.a-pagination .a-selected ~ .a-normal a');
            if (nextPageButton) {
                nextPageButton.click();
            } else {
                // Send signal that we can stop auto add now
            chrome.runtime.sendMessage({
                data: {
                    type: 'auto-add-product-stop'
                }
            });
            }

            return;
        }

        let product = products[autoAddIndex];

        let _customPrice = 0;
        let _originalPrice = 0;

        try {
            _originalPrice = parseFloat(product.querySelector('.a-price .a-offscreen').innerText.replace('$', ''));
        } catch {
            console.log('111');
            autoAddProduct();
            return;
        } 

        console.log(_originalPrice);

        switch (true) {
            case (_originalPrice < 10): {
                autoAddProduct();
                return;
            }
            case (_originalPrice < 20): {
                _customPrice = 9.95;
                break;
            }
            case (_originalPrice < 30): {
                _customPrice = 12.95;
                break;
            }
            case (_originalPrice < 40): {
                _customPrice = 15.95;
                break;
            }
            case (_originalPrice < 60): {
                _customPrice = 17.95;
                break;
            }
            case (_originalPrice < 100): {
                _customPrice = 19.95;
                break;
            }
            case (_originalPrice < 200): {
                _customPrice = 24.95;
                break;
            }
            case (_originalPrice < 300): {
                _customPrice = 29.95;
                break;
            }
            case (_originalPrice < 400): {
                _customPrice = 39.95;
                break;
            }
            case (_originalPrice < 500): {
                _customPrice = 49.95;
                break;
            }
            case (_originalPrice < 600): {
                _customPrice = 59.95;
                break;
            }
            default: break;
        }

        let _link = product.querySelector('h5 a.a-link-normal').getAttribute('href');
        if (_link.search('https://www.amazon.com') == -1) {
            _link = 'https://www.amazon.com' + _link;
        }

        console.log(_link);

        let _asin = product.getAttribute('data-asin');

        console.log(_asin);

        chrome.runtime.sendMessage({
            data: {
                type: 'add-product',
                link: _link,
                asin: _asin,
                customPrice: _customPrice,
            }  
        });
    }
}